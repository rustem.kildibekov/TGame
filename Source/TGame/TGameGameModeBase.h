// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TGameGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class TGAME_API ATGameGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
